module.exports = {
  important: false,
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        darker: {
          darkest: '#000000', // black-900
          dark: '#1a1a1a', // black-700
          DEFAULT: '#333333', //black-500
          light: '#4d4d4d', //black-300
          lightest: '#666666', //black-100
        },
      },
    },
  },
  variants: {},
  plugins: [],
};
