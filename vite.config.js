import path from 'path';
import vue from '@vitejs/plugin-vue';
import WindiCSS from 'vite-plugin-windicss';
import VueI18n from '@intlify/vite-plugin-vue-i18n';

export default {
  plugins: [
    vue(),
    WindiCSS(),
    VueI18n({
      include: [path.resolve(__dirname, 'locales/**')],
    }),
  ],
  build: {
    minify: true,
    outDir: 'C:\\Users\\asistente\\Apps\\nginx\\html',
  },
};
