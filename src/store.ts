import { reactive, readonly, ref, unref } from 'vue'
import axios from 'axios'
import { State, Room, Item, Container } from './types/store'

let serverUri = location.hostname.includes('localhost') ? 'http://localhost' : `${location.origin}`

export const state = reactive<State>({
  loggedIn: false,
  rooms: [],
})

export async function setLogin(value: boolean) {
  state.loggedIn = value
  await sessionStorage.setItem('login', `${value}`)
}

export async function getLoginState(): Promise<boolean> {
  return await JSON.parse(`${sessionStorage.getItem('login')}`)
}

export async function cleanState() {
  await setLogin(false)
  state.rooms = []
}

export async function getRooms() {
  state.rooms = await axios
    .get(`${serverUri}/api/rooms`)
    .then((res) => {
      return res.data
    })
    .then((data: Array<Room>) => {
      return data
    })
}

export async function updateRoom(params: boxUpdate) {
  let result = await axios.put(`${serverUri}/api/rooms/${params._id}`, params).then((res) => {
    return res.data
  })

  return result
}

export async function deleteRoom(room_id: string) {
  let result = await axios.delete(`${serverUri}/api/rooms/${room_id}`).then((res) => {
    return res.data
  })

  return result
}

export async function addRoom(name: string, type: string) {
  const data = { name: name, type: type }

  const room: Room = await axios.post(`${serverUri}/api/rooms`, data).then((res) => {
    return res.data
  })

  state.rooms.push(room)
}
/** Item Mutators */
export async function addItem(room_id: string, item_data: any) {
  const data: Item = { ...item_data, date_acquired: new Date() }

  return await axios.post(`${serverUri}/api/rooms/${room_id}/item`, data).then((res) => {
    return res.data.content
  })
}

export async function updateItem(params: itemUpdate) {
  return await axios.put(`${serverUri}/api/rooms/${params.room_id}/item`, params).then((res) => {
    return res.data
  })
}

export async function deleteItem(params: itemDelete) {
  return await axios.delete(`${serverUri}/api/rooms/${params.room_id}/item/${params.item_id}`).then((res) => {
    return res.data
  })
}

/** Container Mutators */

export async function addContainer(room_id: string, container_data: any) {
  const data = { name: container_data.name, items: container_data.items }

  return await axios.post(`${serverUri}/api/rooms/${room_id}/container`, data).then((res) => {
    return res.data.content
  })
}

export async function updateContainer(params: containerUpdate) {
  return await axios.put(`${serverUri}/api/rooms/${params.room_id}/container`, params).then((res) => {
    return res.data
  })
}

export async function deleteContainer(params: containerDelete) {
  return await axios.delete(`${serverUri}/api/rooms/${params.room_id}/container/${params.container_id}`).then((res) => {
    return res.data
  })
}
