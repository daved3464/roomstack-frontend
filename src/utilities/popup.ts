import Swal from 'sweetalert2';

export const PopUp = Swal.mixin({
  buttonsStyling: false,
  showDenyButton: true,
  customClass: {
    popup: 'dark-background',
    title: 'dark-text modal-title',
    confirmButton: 'button green w-24',
    denyButton: 'button red w-24',
  },
});

export const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  customClass: {
    popup: 'dark-background',
    title: 'dark-text',
  },
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  },
});
