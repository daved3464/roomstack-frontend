import { createRouter, createWebHashHistory, RouteRecord, RouteRecordRaw } from 'vue-router';

import Login from './views/Login.vue';
import Rooms from './views/Rooms.vue';
import Categories from './views/Categories.vue';
import About from './views/About.vue';

const routes: Array<RouteRecordRaw> = [
  { path: '/', component: Login },
  { path: '/rooms', component: Rooms },
  { path: '/categories', component: Categories },
  { path: '/about', component: About },
];

export default createRouter({
  history: createWebHashHistory(),
  routes,
});
