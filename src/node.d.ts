interface ImportMeta {
  env: {
    GITHUB_AUTH_TOKEN: string;
    NODE_ENV: 'development' | 'production';
    PORT?: string;
    PWD: string;
    VITE_DATABASE_HOST: string;
    DEV: boolean;
  };
}
