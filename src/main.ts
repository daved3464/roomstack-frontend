/** Import WindiCSS */
import 'virtual:windi.css';
import 'virtual:windi-devtools';
/** Import Base Styles */
import './index.css';
/** Import SweetAlert2 Styles */
import 'sweetalert2/dist/sweetalert2.min.css';
/** Import FontAwesome */
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faChevronLeft,
  faTrashAlt,
  faPen,
  faPlus,
  faMinus,
  faTimes,
  faHome,
  faTags,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

/** Vue Imports */
import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';

import App from './App.vue';
import router from './router';

library.add(faPlus, faChevronLeft, faPen, faTimes, faMinus, faTrashAlt, faHome, faTags);

import es from '../locales/es.yml';
import en from '../locales/en.yml';

const messages = { es, en };
const i18n = createI18n({ legacy: false, locale: 'es', messages });

const app = createApp(App);

app.use(router).use(i18n).component('fa-icon', FontAwesomeIcon).mount('#app');
